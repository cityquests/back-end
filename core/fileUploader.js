const AWS = require('aws-sdk');
const bluebird = require('bluebird');

// abstracts function to upload a file returning a promise
module.exports = (buffer, name, type) => {
  // configure the keys for accessing AWS
  AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
  });

  // configure AWS to work with promises
  AWS.config.setPromisesDependency(bluebird);

  // create S3 instance
  const s3 = new AWS.S3();
  let params = {
    ACL: 'public-read',
    Body: buffer,
    Bucket: process.env.AWS_BUCKET_NAME,
    ContentType: type.mime,
    Key: `${name}.${type.ext}`
  };

  return s3.upload(params).promise();
};
