module.exports = {
  mongodbMemoryServerOptions: {
    users: {
      dbName: 'users'
    },
    binary: {
      version: '3.4.3',
      skipMD5: true
    },
    autoStart: false
  }
};