let jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Staff = mongoose.model(process.env.DB_STAFF);
const StaffCategories = mongoose.model(process.env.DB_STAFF_CATEGORIES);
const bcrypt = require('bcrypt');

module.exports = (app) => {
  // login staff
  app.post(`/api/login`, async (req, res) => {
    const { email, password, remember } = req.body;
    if (email && password) {
      let staff = await Staff.findOne({ email: email });
      let staff_categories = await StaffCategories.findOne({ type: staff.category });
      if (email === staff.email && bcrypt.compareSync(password, staff.password)) {
        let token = jwt.sign({
          email: staff.email,
          privilege: staff_categories.privileges,
          category: staff.category,
        },
          process.env.JWT_KEY,
          {
            expiresIn: remember ? '24h' : '1h' // expires in 24 hours
          }
        );
        // return the JWT token for the future API calls
        return res.status(202).json({
          success: true,
          message: 'Authentication successful!',
          token: token
        });
      } else {
        return res.status(403).json({
          success: false,
          message: 'Incorrect email or password'
        }).send();
      }
    } else {
      return res.status(400).json({
        success: false,
        message: 'Authentication failed! Please check the request'
      }).send();
    }
  })
}