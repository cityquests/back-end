const mongoose = require('mongoose');
const GameModel = mongoose.model(process.env.DB_GAMES);

module.exports = (app) => {

  // get game list
  app.get(`/api/game`, async (req, res) => {
    let games = await GameModel.find();
    return res.status(200).send(games);
  });

  // get game by game id
  app.get(`/api/game/:gameId`, async (req, res) => {
    const { gameId } = req.params;
    let games = await GameModel.find({ gameId: gameId }, req.body);

    return res.status(202).send({
      error: false,
      games,
    })
  });

  // get game list by user id
  app.get(`/api/game/:userId`, async (req, res) => {
    const { userId } = req.params;
    let games = await GameModel.find({ userId: userId }, req.body);

    return res.status(202).send({
      error: false,
      games,
    })
  });

  // create game
  app.post(`/api/game`, async (req, res) => {
    let game = await GameModel.create({
      ...req.body,
    });
    return res.status(201).send({
      error: false,
      game
    })
  });

  // update game
  app.put(`/api/game/:id`, async (req, res) => {
    const { id } = req.params;
    let game = await GameModel.findByIdAndUpdate(id, req.body, { new: true });
    return res.status(202).send({
      error: false,
      game
    })

  });

  // delete game
  app.delete(`/api/game/:id`, async (req, res) => {
    const { id } = req.params;
    let game = await GameModel.findByIdAndDelete(id);
    return res.status(202).send({
      error: false,
      game
    })
  });
}