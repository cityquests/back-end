const mongoose = require('mongoose');
const QuestionAreaModel = mongoose.model(process.env.DB_QUESTION_AREA);

module.exports = (app) => {

  // get question_area list
  app.get(`/api/question_area`, async (req, res) => {
    let question_areas = await QuestionAreaModel.find();
    return res.status(200).send(question_areas);
  });

  // get question_area details by areaId email
  app.get(`/api/question_area/:areaId`, async (req, res) => {
    const { areaId } = req.params;
    let question_area = await QuestionAreaModel.findOne({ areaId: areaId });
    return res.status(202).send({
      error: false,
      question_area,
    })
  });

  // create question_area
  app.post(`/api/question_area`, async (req, res) => {
    let question_area = await QuestionAreaModel.create({
      ...req.body,
    });
    return res.status(201).send({
      error: false,
      question_area
    })
  })

  // update question_area by id
  app.put(`/api/question_area/:id`, async (req, res) => {
    const { id } = req.params;
    let question_area = await QuestionAreaModel.findByIdAndUpdate(id, req.body, { new: true });

    return res.status(202).send({
      error: false,
      question_area
    })
  });

  // delete question_area by id
  app.delete(`/api/question_area/:id`, async (req, res) => {
    const { id } = req.params;
    let question_area = await QuestionAreaModel.findByIdAndDelete(id);

    return res.status(202).send({
      error: false,
      question_area
    })
  });
}