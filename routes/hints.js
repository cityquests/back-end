const mongoose = require('mongoose');
const Hints = mongoose.model(process.env.DB_HINTS);
const uuidv4 = require('uuid/v4');
const fileUploader = require('../core/fileUploader');
const fs = require('fs');
const fileType = require('file-type');
const multiparty = require('multiparty');

module.exports = (app) => {

  // get hint list
  app.get(`/api/hint`, async (req, res) => {
    let hints = await Hints.find();
    return res.status(200).send(hints);
  });

  // get hint detail by id
  app.get(`/api/hint/:hintId`, async (req, res) => {
    const { hintId } = req.params;
    let hint = await Hints.findOne({ hintId: hintId });
    return res.status(202).send({
      error: false,
      hint,
    })
  });

  // create hint
  app.post(`/api/hint`, async (request, response) => {
    const form = new multiparty.Form();
    form.parse(request, async (error, fields, params) => {
      if (error) throw new Error(error);
      try {
        const hintId = uuidv4();
        const folderPath = `quest/${fields.questId[0]}/question/${fields.questionId[0]}/hints/`;
        const fileImg = params.img[0];
        const bufferImg = fs.readFileSync(fileImg.path);
        const typeImg = await fileType.fromBuffer(bufferImg);
        const uploadedImg = await fileUploader(bufferImg, `${folderPath}${uuidv4()}`, typeImg);
        
        const hint = await Hints.create({
          description: fields.description[0],
          img: uploadedImg.Key,
          order: parseInt(fields.order[0]),
          type: fields.type[0],
          questionId: fields.questionId[0],
          hintId: hintId,
        });
        
        return response.status(201).send({
          error: false,
          hint
        });
      } catch (error) {
        return response.status(400).send(error);
      }
    });
  })

  // update hint by id
  app.put(`/api/hint/:id`, async (req, res) => {
    const { id } = req.params;
    let hint = await Hints.findByIdAndUpdate(id, req.body, { new: true });
    return res.status(202).send({
      error: false,
      hint
    })

  });

  // delete hint by id
  app.delete(`/api/hint/:id`, async (req, res) => {
    const { id } = req.params;
    let hint = await Hints.findByIdAndDelete(id);
    return res.status(202).send({
      error: false,
      hint
    })
  })
}