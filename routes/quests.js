const mongoose = require('mongoose');
const Quests = mongoose.model(process.env.DB_QUESTS);
const Questions = mongoose.model(process.env.DB_QUESTIONS);
const uuidv4 = require('uuid/v4');
const fileUploader = require('../core/fileUploader');
const fs = require('fs');
const fileType = require('file-type');
const multiparty = require('multiparty');

module.exports = (app) => {

  // get quest list
  app.get(`/api/quest`, async (req, res) => {
    const quests = await Quests.find();
    return res.status(200).send(quests);
  });

  // get question list by quest id
  app.get(`/api/quest/:questId/questions`, async (req, res) => {
    const { questId } = req.params;
    const questons = await Questions.find({ questId: questId });
    return res.status(202).send({
      error: false,
      questons,
    })
  });

  // get quest details by id
  app.get(`/api/quest/:questId`, async (req, res) => {
    const { questId } = req.params;
    const quest = await Quests.findOne({ questId: questId });
    return res.status(202).send({
      error: false,
      quest,
    })
  });

  // post quest
  app.post(`/api/quest`, async (request, response) => {
    const form = new multiparty.Form();
    form.parse(request, async (error, fields, params) => {
      if (error) throw new Error(error);
      try {
        const questId = uuidv4();
        const folderPath = `quest/${questId}/`;
        const fileLogo = params.logo[0];
        const bufferLogo = fs.readFileSync(fileLogo.path);
        const typeLogo = await fileType.fromBuffer(bufferLogo);
        const uploadedLogo = await fileUploader(bufferLogo, `${folderPath}${uuidv4()}`, typeLogo);
        const fileImg = params.img[0];
        const bufferImg = fs.readFileSync(fileImg.path);
        const typeImg = await fileType.fromBuffer(bufferImg);
        const uploadedImg = await fileUploader(bufferImg, `${folderPath}${uuidv4()}`, typeImg);
        
        const quest = await Quests.create({
          title: fields.title[0],
          address: fields.address[0],
          cost: parseInt(fields.cost[0]),
          region: fields.region[0],
          sale: parseInt(fields.sale[0]),
          type: fields.type[0],
          description: fields.description[0],
          shotDesc: fields.shotDesc[0],
          map: fields.map[0],
          ending: fields.ending[0],
          logo: uploadedLogo.Key,
          img: uploadedImg.Key,
          questId: questId,
          status: 'not_published',
        });
        
        return response.status(201).send({
          error: false,
          quest
        });
      } catch (error) {
        return response.status(400).send(error);
      }
    });
  })

  // update quest by id
  app.put(`/api/quest/:id`, async (req, res) => {
    const { id } = req.params;
    const quest = await Quests.findByIdAndUpdate(id, req.body, { new: true });
    return res.status(202).send({
      error: false,
      quest
    })

  });

  // delete quest by id
  app.delete(`/api/quest/:id`, async (req, res) => {
    const { id } = req.params;
    const quest = await Quests.findByIdAndDelete(id);
    return res.status(202).send({
      error: false,
      quest
    })
  })
}