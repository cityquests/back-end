const mongoose = require('mongoose');
const QuestionAreaModel = mongoose.model(process.env.DB_QUESTION_AREA);
const ImageModel = mongoose.model(process.env.DB_IMAGES);
const uuidv4 = require('uuid/v4');
const fileUploader = require('../core/fileUploader');
const fs = require('fs');
const fileType = require('file-type');
const multiparty = require('multiparty');

module.exports = (app) => {

  // get image list
  app.get(`/api/image`, async (req, res) => {
    let images = await ImageModel.find();
    return res.status(200).send(images);
  });

  // get paginated list
  app.get(`/api/image/:areaId/:page`, async (req, res) => {
    const { areaId } = req.params;
    const resPerPage = 9; // results per page
    const page = req.params.page || 1; // Page
    try {
      const foundImages = await ImageModel.find({ areaId: areaId })
        .skip((resPerPage * page) - resPerPage)
        .limit(resPerPage);
      const numOfImages = await ImageModel.countDocuments({ areaId: areaId });
      return res.status(200).send({
        imagesList: foundImages,
        currentPage: parseInt(page),
        pages: Math.ceil(numOfImages / resPerPage),
        numOfResults: numOfImages
      });
    } catch (err) {
      throw new Error(err);
    }
  });

  // get image details by imageId email
  app.get(`/api/image/:imageId`, async (req, res) => {
    const { imageId } = req.params;
    let image = await ImageModel.findOne({ imageId: imageId });
    return res.status(202).send({
      error: false,
      image,
    })
  });

  // create image
  app.post(`/api/image`, async (request, response) => {
    // upload file
    const form = new multiparty.Form();
    form.parse(request, async (error, fields, params) => {
      if (error) throw new Error(error);
      try {
        const location = fields.location[0].split(',').map(item => Number(item));
        const areaId = parseInt(fields.areaId[0]);
        const typeId = parseInt(fields.typeId[0]);
        const name = fields.name[0];
        // get question area name
        const question_area = await QuestionAreaModel.findOne({ areaId: areaId });
        const local_area = question_area.name.split(' ').join('');
        const file = params.url[0];
        const fileName = `jp/tokyo/${local_area.toLowerCase()}/${uuidv4()}`;
        const path = file.path;
        const buffer = fs.readFileSync(path);
        const type = await fileType.fromBuffer(buffer);
        const uploadedFile = await fileUploader(buffer, fileName, type);
        // update db
        let image = await ImageModel.create({
          imageId: uuidv4(),
          areaId: areaId,
          typeId: typeId,
          location: location,
          name: name,
          url: uploadedFile.Key,
        });
        return response.status(201).send({
          error: false,
          image
        })
      } catch (error) {
        return response.status(400).send(error);
      }
    });
  })

  // update image by id
  app.put(`/api/image/:id`, async (req, res) => {
    const { id } = req.params;
    let image = await ImageModel.findByIdAndUpdate(id, req.body, { new: true });

    return res.status(202).send({
      error: false,
      image
    })
  });

  // delete image by id
  app.delete(`/api/image/:id`, async (req, res) => {
    const { id } = req.params;
    let image = await ImageModel.findByIdAndDelete(id);

    return res.status(202).send({
      error: false,
      image
    })
  });
}