const mongoose = require('mongoose');
const UserModel = mongoose.model(process.env.DB_USERS);
const bcrypt = require('bcrypt');

module.exports = (app) => {

  // get user list
  app.get(`/api/user`, async (req, res) => {
    let users = await UserModel.find();
    return res.status(200).send(users);
  });

  // get user details by user email
  app.get(`/api/user/:email`, async (req, res) => {
    const { email } = req.params;
    let user = await UserModel.findOne({ email: email });
    return res.status(202).send({
      error: false,
      user,
    })
  });

  // create user
  app.post(`/api/user`, async (req, res) => {
    const hash = bcrypt.hashSync(req.body.password, 12);
    let user = await UserModel.create({
      ...req.body,
      password: hash,
    });
    return res.status(201).send({
      error: false,
      user
    })
  })

  // update user by id
  app.put(`/api/user/:id`, async (req, res) => {
    const { id } = req.params;
    let user = await UserModel.findByIdAndUpdate(id, req.body, { new: true });

    return res.status(202).send({
      error: false,
      user
    })
  });

  // delete user by id
  app.delete(`/api/user/:id`, async (req, res) => {
    const { id } = req.params;
    let user = await UserModel.findByIdAndDelete(id);

    return res.status(202).send({
      error: false,
      user
    })
  });
}