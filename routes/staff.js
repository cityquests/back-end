const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Staff = mongoose.model(process.env.DB_STAFF);
const bcrypt = require('bcrypt');

module.exports = (app) => {

  // get staff list
  app.get(`/api/staff`, async (req, res) => {
    let staff = await Staff.find({}, {
      name: 1, email: 1, category: 1, phone: 1, _id: 0
    });
    return res.status(200).send(staff);
  });

  // get staff detail by token
  app.get(`/api/staff_detail`, async (req, res) => {
    let token = req.headers['authorization'] || '';
    token = token.slice(7, token.length);
    var decoded = jwt.verify(token, process.env.JWT_KEY);

    let staff = await Staff.findOne({ email: decoded.email }, {
      name: 1, email: 1, category: 1, phone: 1, _id: 0
    });
    return res.status(202).send({
      error: false,
      staff,
    })
  });

  // get staff detail by email
  app.get(`/api/staff/:email`, async (req, res) => {
    const { email } = req.params;
    let staff = await Staff.findOne({ email: email }, {
      name: 1, email: 1, category: 1, phone: 1, _id: 0
    });
    return res.status(202).send({
      error: false,
      staff,
    })
  });

  // create staff
  app.post(`/api/staff`, async (req, res) => {
    const hash = bcrypt.hashSync(req.body.password, 12);
    let staff = await Staff.create({
      ...req.body,
      password: hash,
    });
    return res.status(201).send({
      error: false,
      staff
    })
  })

  // update staff by id
  app.put(`/api/staff/:id`, async (req, res) => {
    const { id } = req.params;
    let staff = await Staff.findByIdAndUpdate(id, req.body, { new: true });
    return res.status(202).send({
      error: false,
      staff
    })

  });

  // delete staff by id
  app.delete(`/api/staff/:id`, async (req, res) => {
    const { id } = req.params;
    let staff = await Staff.findByIdAndDelete(id);
    return res.status(202).send({
      error: false,
      staff
    })
  })
}