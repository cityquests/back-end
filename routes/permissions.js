const mongoose = require('mongoose');
const PermissionsModel = mongoose.model(process.env.DB_PERMISSIONS);

module.exports = (app) => {
  // get permissions by user role
  app.get(`/api/permissions`, async (req, res) => {
    const { category } = req.decoded;
    let permissions = await PermissionsModel.findOne({ role: category }, {
      _id: 0
    });
    return res.status(202).send({
      error: false,
      permissions: permissions.get('permissions'),
      role: permissions.get('role'),
    })
  });
}