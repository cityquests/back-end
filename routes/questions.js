const mongoose = require('mongoose');
const Questions = mongoose.model(process.env.DB_QUESTIONS);
const Hints = mongoose.model(process.env.DB_HINTS);
const uuidv4 = require('uuid/v4');
const fileUploader = require('../core/fileUploader');
const fs = require('fs');
const fileType = require('file-type');
const multiparty = require('multiparty');

module.exports = (app) => {

  // get question list
  app.get(`/api/question`, async (req, res) => {
    const questions = await Questions.find();
    return res.status(200).send(questions);
  });

  // get question details by id
  app.get(`/api/question/:questionId`, async (req, res) => {
    const { questionId } = req.params;
    const question = await Questions.findOne({ questionId: questionId });
    return res.status(202).send({
      error: false,
      question,
    })
  });

  // get hint list by question id
  app.get(`/api/question/:questionId/hints`, async (req, res) => {
    const { questionId } = req.params;
    const hints = await Hints.find({ questionId: questionId });
    return res.status(202).send({
      error: false,
      hints,
    })
  });

  // create question
  app.post(`/api/question`, async (request, response) => {
    const form = new multiparty.Form();
    form.parse(request, async (error, fields, params) => {
      if (error) throw new Error(error);
      try {
        const questionId = uuidv4();
        const folderPath = `quest/${fields.questId[0]}/question/`;
        const fileImg = params.img[0];
        const bufferImg = fs.readFileSync(fileImg.path);
        const typeImg = await fileType.fromBuffer(bufferImg);
        const uploadedImg = await fileUploader(bufferImg, `${folderPath}${questionId}`, typeImg);
        
        const question = await Questions.create({
          description: fields.description[0],
          answer: fields.answer[0],
          fact: fields.fact[0],
          order: parseInt(fields.order[0]),
          map: fields.map[0],
          type: fields.type[0],
          questId: fields.questId[0],
          img: uploadedImg.Key,
          questionId: questionId,
          status: 'not_published',
        });
        
        return response.status(201).send({
          error: false,
          question
        });
      } catch (error) {
        return response.status(400).send(error);
      }
    });
  })

  // update question by id
  app.put(`/api/question/:id`, async (req, res) => {
    const { id } = req.params;
    const question = await Questions.findByIdAndUpdate(id, req.body, { new: true });
    return res.status(202).send({
      error: false,
      question
    })

  });

  // delete question by id
  app.delete(`/api/question/:id`, async (req, res) => {
    const { id } = req.params;
    const question = await Questions.findByIdAndDelete(id);
    return res.status(202).send({
      error: false,
      question
    })
  })
}