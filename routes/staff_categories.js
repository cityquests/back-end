const mongoose = require('mongoose');
const StaffCategories = mongoose.model(process.env.DB_STAFF_CATEGORIES);

module.exports = (app) => {

  // get staff categories list
  app.get(`/api/staff_categories`, async (req, res) => {
    let staff_categories = await StaffCategories.find({}, { _id: 0, __v: 0 });
    return res.status(200).send(staff_categories);
  });

  // get staff category detail by type
  app.get(`/api/staff_categories/:type`, async (req, res) => {
    const { type } = req.params;
    let staff_categories = await StaffCategories.findOne({ type: type }, { _id: 0 });
    return res.status(202).send({
      error: false,
      staff_categories,
    })
  });

  // add staff category
  app.post(`/api/staff_categories`, async (req, res) => {
    let staff_categories = await StaffCategories.create({
      ...req.body,
    });
    return res.status(201).send({
      error: false,
      staff_categories
    })
  })

  // update staff category by id
  app.put(`/api/staff_categories/:id`, async (req, res) => {
    const { id } = req.params;
    let staff_categories = await StaffCategories.findByIdAndUpdate(id, req.body, { new: true });
    return res.status(202).send({
      error: false,
      staff_categories
    })

  });

  // delete staff category by id
  app.delete(`/api/staff_categories/:id`, async (req, res) => {
    const { id } = req.params;
    let staff_categories = await StaffCategories.findByIdAndDelete(id);
    return res.status(202).send({
      error: false,
      staff_categories
    })
  })
}