const mongoose = require('mongoose');
const QuestionTypeModel = mongoose.model(process.env.DB_QUESTION_TYPE);

module.exports = (app) => {

  // get question_type list
  app.get(`/api/question_type`, async (req, res) => {
    let question_types = await QuestionTypeModel.find();
    return res.status(200).send(question_types);
  });

  // get question_type details by typeId email
  app.get(`/api/question_type/:typeId`, async (req, res) => {
    const { typeId } = req.params;
    let question_type = await QuestionTypeModel.findOne({ typeId: typeId });
    return res.status(202).send({
      error: false,
      question_type,
    })
  });

  // create question_type
  app.post(`/api/question_type`, async (req, res) => {
    let question_type = await QuestionTypeModel.create({
      ...req.body,
    });
    return res.status(201).send({
      error: false,
      question_type
    })
  })

  // update question_type by id
  app.put(`/api/question_type/:id`, async (req, res) => {
    const { id } = req.params;
    let question_type = await QuestionTypeModel.findByIdAndUpdate(id, req.body, { new: true });

    return res.status(202).send({
      error: false,
      question_type
    })
  });

  // delete question_type by id
  app.delete(`/api/question_type/:id`, async (req, res) => {
    const { id } = req.params;
    let question_type = await QuestionTypeModel.findByIdAndDelete(id);

    return res.status(202).send({
      error: false,
      question_type
    })
  });
}