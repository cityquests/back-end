require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const logger = require('morgan');
const middleware = require('./middleware');

// DB connect
require("./db");

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// IMPORT MODELS
require('./models/users');
require('./models/quests');
require('./models/questions');
require('./models/games');
require('./models/hints');
require('./models/staff');
require('./models/staff_categories');
require('./models/question_area');
require('./models/question_type');
require('./models/images');
require('./models/permissions');

require('./routes/admin_auth')(app);
app.use(middleware.checkToken);
//IMPORT ROUTES
require('./routes/users')(app);
require('./routes/quests')(app);
require('./routes/questions')(app);
require('./routes/games')(app);
require('./routes/hints')(app);
require('./routes/staff')(app);
require('./routes/staff_categories')(app);
require('./routes/question_area')(app);
require('./routes/question_type')(app);
require('./routes/images')(app);
require('./routes/permissions')(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
