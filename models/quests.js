const mongoose = require('mongoose');
const { Schema } = mongoose;

const questsSchema = new Schema({
  questId: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    minlength: 3,
  },
  title: { type: String, required: true },
  address: { type: String, required: true },
  logo: { type: String, required: true },
  img: { type: String, required: true },
  cost: { type: Number, required: true },
  region: { type: String, required: true },
  sale: { type: Number, required: true },
  type: { type: String, required: true },
  description: { type: String, required: true },
  shotDesc: { type: String, required: true },
  map: { type: String, required: true },
  ending: { type: String, required: true },
  status: { type: String, required: true },
}, {
  timestamps: true,
});

mongoose.model(process.env.DB_QUESTS, questsSchema);