const mongoose = require('mongoose');
const { Schema } = mongoose;

const questionsSchema = new Schema({
  questionId: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    minlength: 3,
  },
  description: { type: String, required: true },
  answer: { type: String, required: true },
  fact: { type: String, required: true },
  order: { type: Number, required: true },
  img: { type: String, required: true },
  map: { type: String, required: true },
  type: { type: String, required: true },
  questId: { type: String, required: true },
}, {
  timestamps: true,
});

mongoose.model(process.env.DB_QUESTIONS, questionsSchema);