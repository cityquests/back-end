const mongoose = require('mongoose');
const { Schema } = mongoose;

const usersSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    minlength: 3,
  },
  password: { type: String, required: true },
  fullname: { type: String, required: false },
  nickname: { type: String, required: true },
  description: { type: String, required: false },
  phone: {
    type: String,
    required: false,
    unique: true,
    trim: true,
    minlength: 3,
  },
  birthday: { type: Date, required: false },
  lastLogin: { type: Date, required: false },
}, {
  timestamps: true,
});

mongoose.model(process.env.DB_USERS, usersSchema);