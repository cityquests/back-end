const mongoose = require('mongoose');
const { Schema } = mongoose;

const questionAreaSchema = new Schema({
  areaId: {
    type: Number,
    required: true,
    unique: true,
  },
  name: { type: String, required: true },
});

mongoose.model(process.env.DB_QUESTION_AREA, questionAreaSchema);