const mongoose = require('mongoose');
const { Schema } = mongoose;

const staffCategoriesSchema = new Schema({
  type: { type: String, required: true },
  displayName: { type: String, required: true },
  privileges: { type: Number, required: true },
}, {
  timestamps: true,
});

mongoose.model(process.env.DB_STAFF_CATEGORIES, staffCategoriesSchema);