const mongoose = require('mongoose');
const { Schema } = mongoose;

const hintsSchema = new Schema({
  hintId: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    minlength: 3,
  },
  description: { type: String, required: true },
  img: { type: String, required: true },
  order: { type: Number, required: true },
  type: { type: String, required: true },
  questionId: { type: String, required: true },
}, {
  timestamps: true,
});

mongoose.model(process.env.DB_HINTS, hintsSchema);