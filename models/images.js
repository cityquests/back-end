const mongoose = require('mongoose');
const { Schema } = mongoose;

const imagesSchema = new Schema({
  imageId: {
    type: String,
    required: true,
    unique: true,
  },
  url: { type: String, required: true },
  areaId: { type: Number, required: true },
  typeId: { type: Number, required: true },
  location: { type: [Number], required: true },
  name: { type: String, required: true },
});

mongoose.model(process.env.DB_IMAGES, imagesSchema);