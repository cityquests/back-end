const mongoose = require('mongoose');
const { Schema } = mongoose;

const questionTypeSchema = new Schema({
  typeId: {
    type: Number,
    required: true,
    unique: true,
  },
  name: { type: String, required: true },
});

mongoose.model(process.env.DB_QUESTION_TYPE, questionTypeSchema);