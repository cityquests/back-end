const mongoose = require('mongoose');
const { Schema } = mongoose;

const permissionsSchema = new Schema({
  role: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    minlength: 3,
  },
  permissions: { type: Object, required: true },
});

mongoose.model(process.env.DB_PERMISSIONS, permissionsSchema);