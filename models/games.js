const mongoose = require('mongoose');
const { Schema } = mongoose;

const gameSchema = new Schema({
  gameId: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    minlength: 3,
  },
  userId: { type: String, required: true },
  questId: { type: String, required: true },
  questionId: { type: String, required: true },
  password: { type: String, required: true },
  status: { type: String, required: true },
  score: { type: Number, required: true },
  hints_used: { type: Number, required: true },
  last_login: { type: Date, required: true },
  finished_at: { type: Date, required: true },
}, {
  timestamps: true,
});

mongoose.model(process.env.DB_GAMES, gameSchema);