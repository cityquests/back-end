const mongoose = require('mongoose');
const { Schema } = mongoose;

const staffSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  category: { type: String, required: true },
  lastLoging: { type: Date, required: false },
  phone: { type: String, required: true },
}, {
  timestamps: true,
});

mongoose.model(process.env.DB_STAFF, staffSchema);